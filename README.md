Full's Employee Self Service App
=============

# Introduction

This is a simple Employee Self Service app based on these technologies:

- Twitter Bootstrap 3  
- PHP 5
- MySQL 5

# Install

- Copy files under /public folder to your webserver
- Visit your website /setup.html to start setup the app
- Then you are all set

# About

- Wiki:    https://bitbucket.org/sirius/ess-full/wiki
- Author:  Yu Full Chong <chongyufull@gmai..com>
- Latest Version: v0.1 build 20150916