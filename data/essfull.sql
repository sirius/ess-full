create database essfull
default character set utf8;

use essfull;

CREATE TABLE user
    (
        /** for simplicity design we just use auto increment */
        id INTEGER NOT NULL auto_increment,
        name VARCHAR(255),
        email VARCHAR(255),
        /** password md5 hash */
        password VARCHAR(32),
        /** csv string of possible roles including Employee,Manager,Admin */
        roles VARCHAR(32),
        /** 0=this user has been disabled  */
        enabled BIT,
        /** last login time stamp */
        last_login BIGINT,
        /** last login ip */
        last_ip VARCHAR(32),
        PRIMARY KEY(id),
        unique key email(email(255))
    );

CREATE TABLE leave_request
    (
        id INTEGER NOT NULL auto_increment,
        requestor_id INTEGER,
        approver_id INTEGER,
        /** start and end timestamp */
        start_time BIGINT,
        end_time BIGINT,
        note TEXT,
        /** request timestamp */
        request_time BIGINT,
        /** request timestamp */
        response_time BIGINT,
        /** status=[New|Approved|Rejected] */
        status VARCHAR(16),
        PRIMARY KEY(id)
    );

CREATE TABLE request_log
    (
        id INTEGER NOT NULL auto_increment,
        request_id INTEGER,
        user_id INTEGER,
        /** possible actions=[Create|Approve|Reject]*/
        action VARCHAR(16),
        log_time BIGINT,
        PRIMARY KEY(id)
    );


ALTER TABLE leave_request
ADD INDEX fk_requestor(requestor_id),
    ADD CONSTRAINT fk_requestor FOREIGN KEY(requestor_id) REFERENCES user(id);

ALTER TABLE leave_request
ADD INDEX fk_approver(approver_id),
    ADD CONSTRAINT fk_approver FOREIGN KEY(approver_id) REFERENCES user(id);

ALTER TABLE request_log
ADD INDEX fk_request(request_id),
    ADD CONSTRAINT fk_request FOREIGN KEY(request_id) REFERENCES
leave_request(id);

ALTER TABLE request_log
ADD INDEX fk_user(user_id),
    ADD CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES
user(id);

/** insert sample user data */
insert into user(name, email, password, roles) values('Da Chen', 'da.chenda@gmail.com', md5('abc123'), 'Employee,Manager,Admin'), ('Yu Full Chong', 'chongyufull@gmail.com', md5('abc123'), 'Employee,Manager,Admin'), ('Manager', 'manager@ess.com', md5('abc123'), 'Manager'), ('Admin', 'admin@ess.com', md5('abc123'), 'Admin'), ('Employee', 'employee@ess.com', md5('abc123'), 'Employee');