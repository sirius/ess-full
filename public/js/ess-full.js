/*!
 * JavaScript for ESS App 
 * @author: Yu Full Chong <chongyufull@sap.com>
 */
 var EssFull = {
    
    /* ui logics */
    ui : {
        // UI definition
    	
        brand   : "ELMS",

        footer  : 'Created by &copy; <a href="mailto:chongyufull@gmail.com">Yu Full Chong</a> v0.1 build 20150917',
        
        menus : [
            //{label:"Getting Started", link:"./starter.html"}, 
            {label:"Employee",                  link:"./employee.html",     children: [
                {label:"Create Leave Request",  link:"./employee.html#create-request"},
                {label:"My Leave Requests", link:"./employee.html#review-requests"},
            ]},
            {label:"Manager",                   link:"./manager.html",      children: [
                {label:"Approve Requests",      link:"./manager.html#approve-request"},
                {label:"My Approved Requests",   link:"./manager.html#approved-requests"},
            ]},
            {label:"Admin Tools",               link:"./admin.html",        children: [
                {label:"User Management",       link:"./admin.html#user"},
            ]},
        ],
        
    },
    
    initUI : function($) {

    	// put content into brand
    	$('.navbar-brand').html(this.ui.brand);    	

        // footer
    	$('#footer').find(".footer-text").html(this.ui.footer);

        // build menus
    	var menuHTML = "";
    	for (var i = 0; i < this.ui.menus.length; i++ ) {
    		var menu = this.ui.menus[i];
            if ( !menu.children ) {
                // single level menu
                menuHTML += '<li><a href="' + menu.link+'" '+(menu.target?'target="'+menu.target+'"':'')+'>' + menu.label +'</a></li>';
            } else {
                // 2 level menu with child items
                menuHTML += '<li class="dropdown">';
                menuHTML += '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'+menu.label+' <span class="caret"></span></a>';
                menuHTML += '<ul class="dropdown-menu">';
                for ( var j = 0; j < menu.children.length; j++ ) {
                    var subMenu =  menu.children[j];
                    menuHTML += ' <li><a href="'+subMenu.link+'">'+subMenu.label+'</a></li>';
                }
               menuHTML += '</ul></li>';
            }
    	}
    	$("#navbar").find(".navbar-nav").html(menuHTML); 	
    },
    
    /* backend logics */
    backend : {
    	
    	/** url prefix for all backend service calls*/
    	urlPrefix : "./json/",
    	
    	/**
    	 * call backend, need to provide:
    	 * @param obj.notifier jQuery object to indicate that backend is called, if startWorking is defined the method will be called
    	 * @param obj.initiator jQuery object to initiate this call (typically a button), if specified we will toggle working/no working state
    	 * @param obj.servicePath service URL to call for example 'traceSystem/list'
    	 * @param obj.resultCallback callback function to handle result
    	 * @param obj.errorCallback error callback function to handle server error
    	 * @param obj.successCallback callback function to indicate call is completed
    	 * */
    	callBackend : function(obj) {
    		//debugger;
    		obj.processing = true;
    		if ( obj.initiator ) {
    			obj.initiator.toggleWorking(true);
    		}
    		var url = this.urlPrefix + obj.servicePath;
    		$.support.cors = true;
    		var callObject = {
    			url: url,
    			type: obj.type?obj.type:"get",
    			dataType: "json",       
    			cache: false,
    			headers: {
    		       // "ua": getUserAgentString()
    	    	},
    			beforeSend: function(xhr){
    	       		xhr.withCredentials = true;
    	    	},
    			success: function(result) {
    				//debugger;    				
    				obj.processing = false;
    				if ( obj.initiator ) 
    					obj.initiator.toggleWorking(false);    
    				if ( obj.notifier ) 
    					obj.notifier.hide(); // you can show it again if there's any error or success message want to show
    				if (obj.resultCallback)
    					obj.resultCallback(result, obj);
    				if (obj.completeCallback)
    					obj.completeCallback(obj);	
    			},
    			error: function(e) {
    				//debugger
    				obj.processing = false;
    				if ( obj.initiator ) 
    	    			obj.initiator.toggleWorking(false);
    				if ( obj.notifier ) {        					
    					if ( e.status && e.status >= 400 && e.status < 500 )
    						obj.notifier.showWarning(e);
    					else
    						obj.notifier.showError(e);
    				}
    				if (obj.errorCallback) 
    					obj.errorCallback(e, obj);
    				if (obj.completeCallback)
    					obj.completeCallback(obj);	
    			}
    		};
    		if (obj.data)
    			callObject.data = obj.data;
    		if (obj.contentType)
    			callObject.contentType = obj.contentType;
    		$.ajax(callObject);
    	},
    	
    	/** list trace systems, renderCallback will process a list of TraceSystem (null if failed), errorCallback will handle server error object  */
    	listTechSystems : function(obj) {
    		obj.servicePath = "techSystem/list";
    		this.callBackend(obj);
    	},
    	    	
    }, // end of backend   
};
 

/**
 * jQuery extension methods to indicate/notify working status
 * */
var FullNotifier = {
	toggleWorking : function(working) {
		if ( working ) {
			this.find(".ready").hide();
			this.find(".working").show();
		} else {
			this.find(".ready").show();
			this.find('.working').hide();
		}
		this.prop("disabled",working);
	},
    showProgress : function(str) {
    	this.find(".alert").remove();    
    	this.html('<span class="alert alert-progress">'+str+' <span class="loading-icon"><img src="./img/loading.gif" /></span></span>');
    	this.show().delay(1000);
    },
    showError : function(error) {
    	this.find(".alert").remove();    	
    	this.find(".alert-progress").remove();  
    	var message = "";
    	if ( error.responseText ){
    		var title = AsUtil.extractErrorTitle(error.responseText);
    		if ( title )
    			message = title;
    		else
    			message = error.statusText;
    	} 
    	if ( !message && error.statusText ) {
    		message = error.statusText;
    	} 
    	if ( !message ){
    		message = error;    		
    	}
    	var errorArea = jQuery('<div class="alert alert-danger">'+message+'</div>');
    	this.append(errorArea);
    	this.show();
    	
    },
    showWarning : function(error) {
    	this.find(".alert").remove();    
    	this.find(".alert-progress").remove();
    	var message = "";
    	if ( error.responseText ){
    		var title = AsUtil.extractErrorTitle(error.responseText);
    		if ( title )
    			message = title;
    		else
    			message = error.statusText;
    	} 
    	if ( !message && error.statusText ) {
    		message = error.statusText;
    	} 
    	if ( !message ){
    		message = error;    		
    	}
    	var warnArea = jQuery('<div class="alert alert-warning">'+message+'</div>');
    	this.append(warnArea);
    	this.show();
    	
    },
    showSuccess : function(message, detail) {
    	this.find(".alert").remove();
    	this.find(".alert-progress").remove();
    	if ( !message ) {
    		message = "Operation completed successfully";
    	}
    	var successArea = jQuery('<div class="alert alert-success">'+message+'</div>');
    	this.append(successArea);
    	this.show();
    },

};

// ui initialization logic
!function($) {
    "use strict";
    $(function() {

        // extend jquery first
        $.fn.extend(FullNotifier);

        // then init our ui
    	EssFull.initUI($);
    	
    });
}(jQuery);